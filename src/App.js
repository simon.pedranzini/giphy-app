import React, { useState } from 'react';
import { Link, Route, useLocation } from 'wouter';
import './App.css';
import ListOfGifs from './components/ListOfGifs/ListOfGifs';

function App() {

  const [keyword, setKeyword] = useState('');
  const [path, pushLocation] = useLocation();


  const handleSubmit = evt => {
 //navegar a otra ruta
 evt.preventDefault();
 pushLocation(`/gif/${keyword}`)

  }

  const handleChange = evt => {
    setKeyword(evt.target.value)
  }

  return (
    <div className="App">
      <h1 style={{color: 'blue'}}>Giphy APP</h1>
        {/* <button onClick={() => setKeyword('rainbow')}>RANDOMGIF'S</button> */}
      <section className='App-content'>
        <form onSubmit={handleSubmit}>
          Busca un gif🔎: <input onChange={handleChange} type="text" value={keyword}></input>
          </form>
        <Route path='/gif/:keyword' component={ListOfGifs} />
      </section>
    </div>
  );
}

export default App;

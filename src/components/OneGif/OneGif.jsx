import './OneGif.css'
export default function ShowOneGif ({title, id, url}) {

    return <div className='gif-card'>
    <h4>{title}</h4>
    <a href={`#${id}`}><img className='App-gif' alt={title} src={url} /></a>
    <p><small>ID: {id}</small></p>
    </div>
}

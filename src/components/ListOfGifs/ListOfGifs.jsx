import React, {useEffect, useState} from 'react';
import Gif from '../OneGif/OneGif'
import getGifs from '../../services/getGifs'


export default function ShowListOfGifs ({ params }) {
  const { keyword } = params;

  const [gifs, setGifs] = useState(
    { loading: false, results: []}
  );

    useEffect(function () {
      setGifs(
        actualGifs => ({ loading: true, results: actualGifs.results})
        );
        getGifs({keyword }) // LA KEYWORD ES UNA DEPENDENCIA DEL USEFFECT
        .then(gifs => {
          setGifs({ loading: false, results: gifs})
        });
      }, [keyword]); //SI PONEMOS ARRAY VACÍO COMO DEPENDENCIA SOLO SE EJECUTARÁ UNA VEZ!

      if (gifs.loading) return <p>Cargando...</p>

    return gifs.results.map(({id, title, url}) =>
        <Gif
        key={JSON.stringify(id)}
        title={title}
        url={url}
        id={id} />
        )
}